# i.Sec Engineering Entry Task #

# Abstract

Prospective engineer needs to develop a _RESTFul API_ that allows a _client_ to order meals.
An Order consists of Meal name, Serving Size and price. The engineer will also build a secure 
management _UI_ that allows restaurant staff to create meals that can be ordered by client.

# Requirements

## RESTFul API [using Vert.x Toolkit - latest version]

1. `POST /order` - makes an order. use suitable request parameters:
2. `GET /orders/:customerId` - gets all orders by a customer.
3. `DELETE /order/:orderId` - delets an order
3. `PUT /order/:orderId` - edits an order

## Deliverables

1. Gradle based project
2. Unit/Integration test integrated in the build process
3. `Dockerfile` to create ready-to-run docker image
3. `.sql` file to bootstrap database. _MySql_ prefered. 
4. A github link containing the above.

## Management Console [using Play! Framework - latest version]
1. UI to view all orders made. Orders must not be editable from management console
2. UI to create meals. Restaurant manager most approve before meal is available.
3. UI to view all meals
4. UI to update meals
5. UI to delete meals. Only Restaurant manager can delete.

## Deliverables

1. The Play! project
2. `.sql` file to bootstrap database. _MySql_ prefered.
3. A github link containing the above. 

### Non Deliverables
1. Engineer does not need to build a client to make orders.